#!/bin/bash

testsuites_file="${1}"

jq -r '.[] | .name' "${testsuites_file}" | tr '\n' ','
