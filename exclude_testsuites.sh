#!/bin/bash

testsuites="${1}"
exclude_suites="${2}"

while IFS= read -r suite; do
    testsuites="${testsuites/${suite},/}"
done <<< "${exclude_suites}"

echo "${testsuites}"
