FROM ubuntu:focal

ARG VREST_VERSION=2.1.0
USER root

RUN apt update && \
    apt install -y ca-certificates && \
    apt install -y openssl curl gcc git jq bash

RUN VREST_UVERSION=$(echo ${VREST_VERSION} | sed 's/\./_/g') && \
    mkdir -p vrest && \
    curl -Lso /usr/local/bin/vrest https://github.com/Optimizory/vrest-ng-cli/releases/download/v${VREST_VERSION}/vrest_ng_cli_linux_${VREST_UVERSION} && \
    chmod +x /usr/local/bin/vrest

COPY ./get_all_testsuites.sh /usr/local/bin/get_all_testsuites
COPY ./exclude_testsuites.sh /usr/local/bin/exclude_testsuites